# TL;DR Why choose this 
This fork also builds upon the current infrastructure to provide an experience where the user can search youtube and view the thumbnail & title and choose to download a video. This project is forked from [recodeuk](https://github.com/recodeuk/youtube-dl-flask) and heavily based on [youtube-dl](https://github.com/ytdl-org/youtube-dl)


## Running

### Docker CLI

This example uses the docker run command to create the container to run the app. Here we also use host networking for simplicity. Also note the `-v` argument. This directory will be used to output the resulting videos

```shell
docker run -d -p 8080:8080 --name youtube-dl -v /home/core/youtube-dl:/youtube-dl splazit/youtube-dl-ui
```

## Usage

### Start a download remotely

Downloads can be triggered by supplying the `youtube url` of the requested video through the Web UI or through the REST interface via curl, etc.

#### Web browser

Just navigate to `http://localhost:8080/youtube-dl` 

#### Curl

```shell
curl -X POST --data-urlencode "url=<youtube url>" http://localhost:8080/youtube-dl/q
```

#### Fetch

```javascript
fetch(`http://${host}:8080/youtube-dl/q`, {
  method: "POST",
  body: new URLSearchParams({
    url: url,
    format: "bestvideo"
  }),
});
```
